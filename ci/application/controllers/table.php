<?php 
	
	/**
	 * 
	 */
	class table extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model('Table_model');
			$this->load->library('form_validation');
 
		}
		
		public function index()
		{

			
			$data['judul'] = 'Daftar Akun';
			 $data['table'] = $this->Table_model->getAllTable();
			//$data['list'] = $this->Table_model->get(); //ambil semua data dari table yang ada di database 
			$this->load->view('template/header',$data);
			$this->load->view('table/index', $data);
			$this->load->view('template/footer');
		}

		public function tambah()
		{
			$data['judul'] = 'Form Akun';
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if($this->form_validation->run() == FALSE){
			$this->load->view('template/header', $data);
			$this->load->view('table/tambah');
			$this->load->view('template/footer');	
		}else {
				$this->Table_model->tambahAkun();
			 redirect('table');
		}
			
			
		}


	}


 ?>