<div class="container">
	
	<div class="row mt-3 mx-auto">
		<div class="col-md-6">
	
	<div class="card">
		<div class="card-header">Register an Account</div>
		  <div class="card-body">
        <?php if(validation_errors()) : ?>
        <div class="alert alert-danger" role="alert">
        <?= validation_errors(); ?>  
        </div>
		  	<?php endif; ?>
          <form action="" method="post">
          	<div class="form-group">
    			<label for="nama">nama</label>
    			<input type="text" name="nama" class="form-control" id="nama">
  			</div>
  			<div class="form-group">
    			<label for="email">email</label>
    			<input type="text" name="email" class="form-control" id="email">
  			</div>
  			<div class="form-group">
    			<label for="password">password</label>
    			<input type="password" name="password" class="form-control" id="password">
  			</div>
        <button type="submit" name="tambah" class="btn btn-primary btn-block"> Register</button>

          </form>
        
		  </div>
		</div>
		</div>	
	</div>

         <!--  <a class="btn btn-primary btn-block" href="index.php">Register</a> -->

</div>